# Punjabi translation for apport
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the apport package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: apport\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-09-10 11:16+0200\n"
"PO-Revision-Date: 2014-02-02 00:14+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Punjabi <pa@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2019-04-13 05:35+0000\n"
"X-Generator: Launchpad (build 18920)\n"
"Language: pa\n"

#: ../kde/apport-kde.py:469 ../kde/apport-kde.py:504 ../kde/apport-kde.py:524
#: ../debian/tmp/usr/share/apport/apport-kde.py:469
#: ../debian/tmp/usr/share/apport/apport-kde.py:504
#: ../debian/tmp/usr/share/apport/apport-kde.py:524 ../gtk/apport-gtk.ui.h:1
#: ../debian/tmp/usr/share/apport/apport-gtk.ui.h:1
msgid "Apport"
msgstr ""

#: ../gtk/apport-gtk.ui.h:2 ../debian/tmp/usr/share/apport/apport-gtk.ui.h:2
msgid "Cancel"
msgstr ""

#: ../gtk/apport-gtk.ui.h:3 ../debian/tmp/usr/share/apport/apport-gtk.ui.h:3
msgid "OK"
msgstr ""

#: ../gtk/apport-gtk.ui.h:4 ../debian/tmp/usr/share/apport/apport-gtk.ui.h:4
msgid "Crash report"
msgstr ""

#: ../gtk/apport-gtk.ui.h:5 ../debian/tmp/usr/share/apport/apport-gtk.ui.h:5
msgid "<big><b>Sorry, an internal error happened.</b></big>"
msgstr ""

#: ../kde/apport-kde.py:241 ../debian/tmp/usr/share/apport/apport-kde.py:241
#: ../gtk/apport-gtk.ui.h:7 ../debian/tmp/usr/share/apport/apport-gtk.py:297
#: ../gtk/apport-gtk.py:297 ../debian/tmp/usr/share/apport/apport-gtk.ui.h:7
msgid "If you notice further problems, try restarting the computer."
msgstr ""

#: ../gtk/apport-gtk.ui.h:7 ../debian/tmp/usr/share/apport/apport-gtk.ui.h:7
msgid "Send an error report to help fix this problem"
msgstr ""

#: ../gtk/apport-gtk.ui.h:9 ../debian/tmp/usr/share/apport/apport-gtk.ui.h:9
msgid "Ignore future problems of this program version"
msgstr ""

#: ../kde/apport-kde.py:292 ../debian/tmp/usr/share/apport/apport-kde.py:292
#: ../gtk/apport-gtk.ui.h:11 ../debian/tmp/usr/share/apport/apport-gtk.py:204
#: ../debian/tmp/usr/share/apport/apport-gtk.py:575 ../gtk/apport-gtk.py:204
#: ../gtk/apport-gtk.py:575 ../debian/tmp/usr/share/apport/apport-gtk.ui.h:11
msgid "Show Details"
msgstr "ਵੇਰਵਾ ਵੇਖੋ"

#: ../gtk/apport-gtk.ui.h:12 ../debian/tmp/usr/share/apport/apport-gtk.ui.h:12
msgid "_Examine locally"
msgstr ""

#: ../kde/apport-kde.py:234 ../debian/tmp/usr/share/apport/apport-kde.py:234
msgid "Leave Closed"
msgstr "ਬੰਦ ਰਹਿਣ ਦਿਓ"

#: ../kde/apport-kde.py:231 ../kde/apport-kde.py:244
#: ../debian/tmp/usr/share/apport/apport-kde.py:231
#: ../debian/tmp/usr/share/apport/apport-kde.py:244
#: ../debian/tmp/usr/share/apport/apport-gtk.py:216 ../gtk/apport-gtk.py:216
msgid "Continue"
msgstr "ਜਾਰੀ ਰੱਖੋ"

#: ../gtk/apport-gtk.ui.h:15 ../debian/tmp/usr/share/apport/apport-gtk.ui.h:15
msgid "<big><b>Collecting problem information</b></big>"
msgstr ""

#: ../gtk/apport-gtk.ui.h:16 ../debian/tmp/usr/share/apport/apport-gtk.ui.h:16
msgid ""
"Information is being collected that may help the developers fix the problem "
"you report."
msgstr ""

#: ../kde/apport-kde.py:435 ../debian/tmp/usr/share/apport/apport-kde.py:435
#: ../bin/apport-cli.py:267 ../gtk/apport-gtk.ui.h:17
#: ../debian/tmp/usr/bin/apport-cli.py:267
#: ../debian/tmp/usr/share/apport/apport-gtk.ui.h:17
msgid "Uploading problem information"
msgstr ""

#: ../gtk/apport-gtk.ui.h:18 ../debian/tmp/usr/share/apport/apport-gtk.ui.h:18
msgid "<big><b>Uploading problem information</b></big>"
msgstr ""

#: ../kde/apport-kde.py:436 ../debian/tmp/usr/share/apport/apport-kde.py:436
#: ../gtk/apport-gtk.ui.h:19 ../debian/tmp/usr/share/apport/apport-gtk.ui.h:19
msgid ""
"The collected information is being sent to the bug tracking system. This "
"might take a few minutes."
msgstr ""

#: ../kde/apport-kde-mimelnk.desktop.in.h:1
msgid "Apport crash file"
msgstr ""

#: ../kde/apport-kde.py:363 ../debian/tmp/usr/share/apport/apport-kde.py:363
#: ../bin/apport-cli.py:166 ../debian/tmp/usr/bin/apport-cli.py:166
#: ../debian/tmp/usr/share/apport/apport-gtk.py:146 ../gtk/apport-gtk.py:146
msgid "(binary data)"
msgstr ""

#: ../debian/tmp/usr/share/apport/apport-gtk.py:161 ../gtk/apport-gtk.py:161
#, python-format
msgid "Sorry, the application %s has stopped unexpectedly."
msgstr ""

#: ../debian/tmp/usr/share/apport/apport-gtk.py:164 ../gtk/apport-gtk.py:164
#, python-format
msgid "Sorry, %s has closed unexpectedly."
msgstr ""

#: ../kde/apport-kde.py:200 ../kde/apport-kde.py:238
#: ../debian/tmp/usr/share/apport/apport-kde.py:200
#: ../debian/tmp/usr/share/apport/apport-kde.py:238
#: ../debian/tmp/usr/share/apport/apport-gtk.py:169 ../gtk/apport-gtk.py:169
#, python-format
msgid "Sorry, %s has experienced an internal error."
msgstr ""

#: ../kde/apport-kde.py:186 ../debian/tmp/usr/share/apport/apport-kde.py:186
#: ../bin/apport-cli.py:194 ../gtk/apport-gtk.ui.h:6
#: ../debian/tmp/usr/bin/apport-cli.py:194
#: ../debian/tmp/usr/share/apport/apport-gtk.py:181 ../gtk/apport-gtk.py:181
#: ../debian/tmp/usr/share/apport/apport-gtk.ui.h:6
msgid "Send problem report to the developers?"
msgstr ""

#: ../kde/apport-kde.py:194 ../debian/tmp/usr/share/apport/apport-kde.py:194
#: ../gtk/apport-gtk.ui.h:14 ../debian/tmp/usr/share/apport/apport-gtk.py:188
#: ../gtk/apport-gtk.py:188 ../debian/tmp/usr/share/apport/apport-gtk.ui.h:14
msgid "Send"
msgstr "ਭੇਜੋ"

#: ../gtk/apport-gtk.py:231 ../debian/tmp/usr/share/apport/apport-gtk.py:231
msgid "Force Closed"
msgstr ""

#: ../kde/apport-kde.py:235 ../kde/apport-kde.py:381
#: ../debian/tmp/usr/share/apport/apport-kde.py:235
#: ../debian/tmp/usr/share/apport/apport-kde.py:381
msgid "Relaunch"
msgstr "ਮੁੜ-ਚਲਾਓ"

#: ../debian/tmp/usr/share/apport/apport-gtk.py:239 ../gtk/apport-gtk.py:239
#, python-format
msgid "The application %s has stopped responding."
msgstr ""

#: ../debian/tmp/usr/share/apport/apport-gtk.py:243 ../gtk/apport-gtk.py:243
#, python-format
msgid "The program \"%s\" has stopped responding."
msgstr ""

#: ../kde/apport-kde.py:208 ../debian/tmp/usr/share/apport/apport-kde.py:208
#: ../debian/tmp/usr/share/apport/apport-gtk.py:259 ../gtk/apport-gtk.py:259
#, python-format
msgid "Package: %s"
msgstr "ਪੈਕੇਜ: %s"

#: ../kde/apport-kde.py:214 ../debian/tmp/usr/share/apport/apport-kde.py:214
#: ../debian/tmp/usr/share/apport/apport-gtk.py:265 ../gtk/apport-gtk.py:265
msgid "Sorry, a problem occurred while installing software."
msgstr "ਅਫਸੋਸ, ਸਾਫਟਵੇਅਰ ਇੰਸਟਾਲ ਕਰਨ ਦੌਰਾਨ ਗਲਤੀ ਆਈ ਹੈ।"

#: ../kde/apport-kde.py:220 ../debian/tmp/usr/share/apport/apport-kde.py:220
#: ../debian/tmp/usr/share/apport/apport-gtk.py:274
#: ../debian/tmp/usr/share/apport/apport-gtk.py:289 ../gtk/apport-gtk.py:274
#: ../gtk/apport-gtk.py:289
#, python-format
msgid "The application %s has experienced an internal error."
msgstr "ਐਪਲੀਕੇਸ਼ਨ %s ਨੂੰ ਅੰਦਰੂਨੀ ਗਲਤੀ ਆਈ ਹੈ।"

#: ../kde/apport-kde.py:223 ../debian/tmp/usr/share/apport/apport-kde.py:223
#: ../debian/tmp/usr/share/apport/apport-gtk.py:277 ../gtk/apport-gtk.py:277
#, python-format
msgid "The application %s has closed unexpectedly."
msgstr "ਐਪਲੀਕੇਸ਼ਨ %s ਅਚਾਨਕ ਬੰਦ ਹੋ ਗਈ ਹੈ।"

#: ../kde/apport-kde.py:245 ../debian/tmp/usr/share/apport/apport-kde.py:245
#: ../debian/tmp/usr/share/apport/apport-gtk.py:299 ../gtk/apport-gtk.py:299
msgid "Ignore future problems of this type"
msgstr ""

#: ../kde/apport-kde.py:289 ../debian/tmp/usr/share/apport/apport-kde.py:289
#: ../debian/tmp/usr/share/apport/apport-gtk.py:579 ../gtk/apport-gtk.py:579
msgid "Hide Details"
msgstr "ਵੇਰਵਾ ਓਹਲੇ"

#: ../debian/tmp/usr/bin/apport-unpack.py:22 ../bin/apport-unpack.py:22
#, python-format
msgid "Usage: %s <report> <target directory>"
msgstr ""

#: ../debian/tmp/usr/bin/apport-unpack.py:42 ../bin/apport-unpack.py:42
msgid "Destination directory exists and is not empty."
msgstr ""

#: ../kde/apport-kde.py:315 ../debian/tmp/usr/share/apport/apport-kde.py:315
msgid "Username:"
msgstr "ਯੂਜ਼ਰ-ਨਾਂ:"

#: ../kde/apport-kde.py:316 ../debian/tmp/usr/share/apport/apport-kde.py:316
msgid "Password:"
msgstr "ਪਾਸਵਰਡ:"

#: ../kde/apport-kde.py:406 ../debian/tmp/usr/share/apport/apport-kde.py:406
msgid "Collecting Problem Information"
msgstr ""

#: ../kde/apport-kde.py:407 ../debian/tmp/usr/share/apport/apport-kde.py:407
#: ../bin/apport-cli.py:254 ../debian/tmp/usr/bin/apport-cli.py:254
msgid "Collecting problem information"
msgstr ""

#: ../kde/apport-kde.py:408 ../debian/tmp/usr/share/apport/apport-kde.py:408
msgid ""
"The collected information can be sent to the developers to improve the "
"application. This might take a few minutes."
msgstr ""

#: ../kde/apport-kde.py:434 ../debian/tmp/usr/share/apport/apport-kde.py:434
msgid "Uploading Problem Information"
msgstr ""

#: ../apport/com.ubuntu.apport.policy.in.h:1
msgid "Collect system information"
msgstr ""

#: ../apport/com.ubuntu.apport.policy.in.h:2
msgid ""
"Authentication is required to collect system information for this problem "
"report"
msgstr ""

#: ../apport/com.ubuntu.apport.policy.in.h:3
msgid "System problem reports"
msgstr ""

#: ../apport/com.ubuntu.apport.policy.in.h:4
msgid ""
"Please enter your password to access problem reports of system programs"
msgstr ""

#: ../kde/apport-kde-mime.desktop.in.h:1 ../kde/apport-kde.desktop.in.h:1
#: ../gtk/apport-gtk.desktop.in.h:1
msgid "Report a problem..."
msgstr "ਸਮੱਸਿਆ ਰਿਪੋਰਟ"

#: ../kde/apport-kde-mime.desktop.in.h:2 ../kde/apport-kde.desktop.in.h:2
#: ../gtk/apport-gtk.desktop.in.h:2
msgid "Report a malfunction to the developers"
msgstr ""

#: ../bin/apport-valgrind.py:37 ../debian/tmp/usr/bin/apport-valgrind.py:37
msgid "See man page for details."
msgstr "ਵਿਸਥਾਰ ਲਈ ਮੈਨ ਪੇਜ ਵੇਖੋ।"

#: ../bin/apport-valgrind.py:43 ../debian/tmp/usr/bin/apport-valgrind.py:43
msgid "specify the log file name produced by valgrind"
msgstr ""

#: ../bin/apport-valgrind.py:46 ../debian/tmp/usr/bin/apport-valgrind.py:46
msgid ""
"reuse a previously created sandbox dir (SDIR) or, if it does not exist, "
"create it"
msgstr ""

#: ../bin/apport-valgrind.py:50 ../debian/tmp/usr/bin/apport-valgrind.py:50
msgid ""
"do  not  create  or reuse a sandbox directory for additional debug symbols "
"but rely only on installed debug symbols."
msgstr ""

#: ../bin/apport-valgrind.py:54 ../debian/tmp/usr/bin/apport-valgrind.py:54
msgid ""
"reuse a previously created cache dir (CDIR) or, if it does not exist, create "
"it"
msgstr ""

#: ../bin/apport-valgrind.py:58 ../debian/tmp/usr/bin/apport-valgrind.py:58
msgid ""
"report download/install progress when installing packages into sandbox"
msgstr ""

#: ../bin/apport-valgrind.py:62 ../debian/tmp/usr/bin/apport-valgrind.py:62
msgid ""
"the executable that is run under valgrind's memcheck tool  for memory leak "
"detection"
msgstr ""

#: ../bin/apport-retrace.py:65 ../bin/apport-valgrind.py:66
#: ../debian/tmp/usr/bin/apport-retrace.py:65
#: ../debian/tmp/usr/bin/apport-valgrind.py:66
msgid ""
"Install an extra package into the sandbox (can be specified multiple times)"
msgstr ""
"ਸੈਂਡ ਬੌਕਸ  ਵਿੱਚ ਵਾਧੂ ਪੈਕਿਜ ਇੰਸਟਾਲ (ਇਸਨੂੰ ਕਈ ਵਾਰ  ਨਿਸ਼ਚਤ ਕੀਤਾ ਜਾ ਸਕਦਾ ਹੈ)"

#: ../bin/apport-valgrind.py:97 ../debian/tmp/usr/bin/apport-valgrind.py:97
#, python-format
msgid "Error: %s is not an executable. Stopping."
msgstr "ਗਲਤੀ: %s ਚੱਲਣਯੋਗ ਨਹੀਂ ਹੈ। ਬੰਦ ਹੋ ਰਿਹਾ ਹੈ।"

#: ../debian/tmp/usr/share/apport/kernel_oops.py:29 ../data/kernel_oops.py:29
msgid "Your system might become unstable now and might need to be restarted."
msgstr ""

#: ../bin/apport-retrace.py:34 ../debian/tmp/usr/bin/apport-retrace.py:34
msgid "Do not put the new traces into the report, but write them to stdout."
msgstr ""

#: ../bin/apport-retrace.py:36 ../debian/tmp/usr/bin/apport-retrace.py:36
msgid ""
"Start an interactive gdb session with the report's core dump (-o ignored; "
"does not rewrite report)"
msgstr ""

#: ../bin/apport-retrace.py:38 ../debian/tmp/usr/bin/apport-retrace.py:38
msgid ""
"Write modified report to given file instead of changing the original report"
msgstr ""

#: ../bin/apport-retrace.py:41 ../debian/tmp/usr/bin/apport-retrace.py:41
msgid "Remove the core dump from the report after stack trace regeneration"
msgstr ""

#: ../bin/apport-retrace.py:43 ../debian/tmp/usr/bin/apport-retrace.py:43
msgid "Override report's CoreFile"
msgstr ""

#: ../bin/apport-retrace.py:45 ../debian/tmp/usr/bin/apport-retrace.py:45
msgid "Override report's ExecutablePath"
msgstr ""

#: ../bin/apport-retrace.py:47 ../debian/tmp/usr/bin/apport-retrace.py:47
msgid "Override report's ProcMaps"
msgstr ""

#: ../bin/apport-retrace.py:49 ../debian/tmp/usr/bin/apport-retrace.py:49
msgid "Rebuild report's Package information"
msgstr ""

#: ../bin/apport-retrace.py:51 ../debian/tmp/usr/bin/apport-retrace.py:51
msgid ""
"Build a temporary sandbox and download/install the necessary packages and "
"debug symbols in there; without this option it assumes that the necessary "
"packages and debug symbols are already installed in the system. The argument "
"points to the packaging system configuration base directory; if you specify "
"\"system\", it will use the system configuration files, but will then only "
"be able to retrace crashes that happened on the currently running release."
msgstr ""

#: ../bin/apport-retrace.py:55 ../debian/tmp/usr/bin/apport-retrace.py:55
msgid ""
"Report download/install progress when installing packages into sandbox"
msgstr ""

#: ../bin/apport-retrace.py:57 ../debian/tmp/usr/bin/apport-retrace.py:57
msgid "Prepend timestamps to log messages, for batch operation"
msgstr ""

#: ../bin/apport-retrace.py:59 ../debian/tmp/usr/bin/apport-retrace.py:59
msgid ""
"Create and use third-party repositories from origins specified in reports"
msgstr ""

#: ../bin/apport-retrace.py:61 ../debian/tmp/usr/bin/apport-retrace.py:61
msgid "Cache directory for packages downloaded in the sandbox"
msgstr ""

#: ../bin/apport-retrace.py:63 ../debian/tmp/usr/bin/apport-retrace.py:63
msgid ""
"Directory for unpacked packages. Future runs will assume that any already "
"downloaded package is also extracted to this sandbox."
msgstr ""

#: ../bin/apport-retrace.py:67 ../debian/tmp/usr/bin/apport-retrace.py:67
msgid ""
"Path to a file with the crash database authentication information. This is "
"used when specifying a crash ID to upload the retraced stack traces (only if "
"neither -g, -o, nor -s are specified)"
msgstr ""

#: ../bin/apport-retrace.py:69 ../debian/tmp/usr/bin/apport-retrace.py:69
msgid ""
"Display retraced stack traces and ask for confirmation before sending them "
"to the crash database."
msgstr ""

#: ../bin/apport-retrace.py:71 ../debian/tmp/usr/bin/apport-retrace.py:71
msgid ""
"Path to the duplicate sqlite database (default: no duplicate checking)"
msgstr ""

#: ../bin/apport-retrace.py:82 ../debian/tmp/usr/bin/apport-retrace.py:82
msgid "You cannot use -C without -S. Stopping."
msgstr ""

#. translators: don't translate y/n, apport currently only checks for "y"
#: ../bin/apport-retrace.py:115 ../debian/tmp/usr/bin/apport-retrace.py:115
msgid "OK to send these as attachments? [y/n]"
msgstr ""

#: ../apport/ui.py:144
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:144
msgid "This package does not seem to be installed correctly"
msgstr ""

#: ../apport/ui.py:149
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:149
#, python-format
msgid ""
"This is not an official %s package. Please remove any third party package "
"and try again."
msgstr ""

#: ../apport/ui.py:166
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:166
#, python-format
msgid ""
"You have some obsolete package versions installed. Please upgrade the "
"following packages and check if the problem still occurs:\n"
"\n"
"%s"
msgstr ""

#: ../apport/ui.py:299
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:299
msgid "unknown program"
msgstr ""

#: ../apport/ui.py:300
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:300
#, python-format
msgid "Sorry, the program \"%s\" closed unexpectedly"
msgstr ""

#: ../apport/ui.py:302 ../apport/ui.py:1412
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:302
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1412
#, python-format
msgid "Problem in %s"
msgstr ""

#: ../apport/ui.py:303
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:303
msgid ""
"Your computer does not have enough free memory to automatically analyze the "
"problem and send a report to the developers."
msgstr ""

#: ../apport/ui.py:358 ../apport/ui.py:366 ../apport/ui.py:503
#: ../apport/ui.py:506 ../apport/ui.py:706 ../apport/ui.py:1218
#: ../apport/ui.py:1384 ../apport/ui.py:1388
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:358
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:366
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:503
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:506
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:706
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1218
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1384
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1388
msgid "Invalid problem report"
msgstr ""

#: ../apport/ui.py:359
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:359
msgid "You are not allowed to access this problem report."
msgstr ""

#: ../apport/ui.py:362
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:362
msgid "Error"
msgstr ""

#: ../apport/ui.py:363
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:363
msgid "There is not enough disk space available to process this report."
msgstr ""

#: ../apport/ui.py:457
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:457
msgid "No package specified"
msgstr ""

#: ../apport/ui.py:458
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:458
msgid ""
"You need to specify a package or a PID. See --help for more information."
msgstr ""

#: ../apport/ui.py:481
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:481
msgid "Permission denied"
msgstr ""

#: ../apport/ui.py:482
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:482
msgid ""
"The specified process does not belong to you. Please run this program as the "
"process owner or as root."
msgstr ""

#: ../apport/ui.py:401 ../apport/ui.py:484
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:401
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:484
msgid "Invalid PID"
msgstr ""

#: ../apport/ui.py:485
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:485
msgid "The specified process ID does not belong to a program."
msgstr ""

#: ../apport/ui.py:504
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:504
#, python-format
msgid "Symptom script %s did not determine an affected package"
msgstr ""

#: ../apport/ui.py:507
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:507
#, python-format
msgid "Package %s does not exist"
msgstr ""

#: ../apport/ui.py:531 ../apport/ui.py:718 ../apport/ui.py:723
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:531
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:718
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:723
msgid "Cannot create report"
msgstr ""

#: ../apport/ui.py:546 ../apport/ui.py:592 ../apport/ui.py:608
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:546
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:592
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:608
msgid "Updating problem report"
msgstr ""

#: ../apport/ui.py:547
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:547
msgid ""
"You are not the reporter or subscriber of this problem report, or the report "
"is a duplicate or already closed.\n"
"\n"
"Please create a new report using \"apport-bug\"."
msgstr ""

#: ../apport/ui.py:556
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:556
msgid ""
"You are not the reporter of this problem report. It is much easier to mark a "
"bug as a duplicate of another than to move your comments and attachments to "
"a new bug.\n"
"\n"
"Subsequently, we recommend that you file a new bug report using \"apport-"
"bug\" and make a comment in this bug about the one you file.\n"
"\n"
"Do you really want to proceed?"
msgstr ""

#: ../apport/ui.py:593 ../apport/ui.py:609
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:593
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:609
msgid "No additional information collected."
msgstr ""

#: ../apport/ui.py:660
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:660
msgid "What kind of problem do you want to report?"
msgstr ""

#: ../apport/ui.py:677
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:677
msgid "Unknown symptom"
msgstr ""

#: ../apport/ui.py:678
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:678
#, python-format
msgid "The symptom \"%s\" is not known."
msgstr ""

#: ../apport/ui.py:709
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:709
msgid ""
"After closing this message please click on an application window to report a "
"problem about it."
msgstr ""

#: ../apport/ui.py:719 ../apport/ui.py:724
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:719
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:724
msgid "xprop failed to determine process ID of the window"
msgstr ""

#: ../apport/ui.py:738
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:738
msgid "%prog <report number>"
msgstr ""

#: ../apport/ui.py:740
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:740
msgid "Specify package name."
msgstr ""

#: ../apport/ui.py:742 ../apport/ui.py:793
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:742
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:793
msgid "Add an extra tag to the report. Can be specified multiple times."
msgstr ""

#: ../apport/ui.py:772
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:772
msgid ""
"%prog [options] [symptom|pid|package|program path|.apport/.crash file]"
msgstr ""

#: ../apport/ui.py:775
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:775
msgid ""
"Start in bug filing mode. Requires --package and an optional --pid, or just "
"a --pid. If neither is given, display a list of known symptoms. (Implied if "
"a single argument is given.)"
msgstr ""

#: ../apport/ui.py:777
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:777
msgid "Click a window as a target for filing a problem report."
msgstr ""

#: ../apport/ui.py:779
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:779
msgid "Start in bug updating mode. Can take an optional --package."
msgstr ""

#: ../apport/ui.py:781
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:781
msgid ""
"File a bug report about a symptom. (Implied if symptom name is given as only "
"argument.)"
msgstr ""

#: ../apport/ui.py:783
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:783
msgid ""
"Specify package name in --file-bug mode. This is optional if a --pid is "
"specified. (Implied if package name is given as only argument.)"
msgstr ""

#: ../apport/ui.py:785
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:785
msgid ""
"Specify a running program in --file-bug mode. If this is specified, the bug "
"report will contain more information.  (Implied if pid is given as only "
"argument.)"
msgstr ""

#: ../apport/ui.py:787
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:787
msgid "The provided pid is a hanging application."
msgstr ""

#: ../apport/ui.py:789
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:789
#, python-format
msgid ""
"Report the crash from given .apport or .crash file instead of the pending "
"ones in %s. (Implied if file is given as only argument.)"
msgstr ""

#: ../apport/ui.py:791
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:791
msgid ""
"In bug filing mode, save the collected information into a file instead of "
"reporting it. This file can then be reported later on from a different "
"machine."
msgstr ""

#: ../apport/ui.py:795
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:795
msgid "Print the Apport version number."
msgstr ""

#: ../apport/ui.py:946
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:946
msgid ""
"This will launch apport-retrace in a terminal window to examine the crash."
msgstr ""

#: ../apport/ui.py:947
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:947
msgid "Run gdb session"
msgstr ""

#: ../apport/ui.py:948
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:948
msgid "Run gdb session without downloading debug symbols"
msgstr ""

#. TRANSLATORS: %s contains the crash report file name
#: ../apport/ui.py:950
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:950
#, python-format
msgid "Update %s with fully symbolic stack trace"
msgstr ""

#: ../apport/ui.py:1046 ../apport/ui.py:1056
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1046
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1056
msgid ""
"This problem report applies to a program which is not installed any more."
msgstr ""

#: ../apport/ui.py:1071
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1071
#, python-format
msgid ""
"The problem happened with the program %s which changed since the crash "
"occurred."
msgstr ""

#: ../apport/ui.py:1118 ../apport/ui.py:1175 ../apport/ui.py:1390
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1118
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1175
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1390
msgid "This problem report is damaged and cannot be processed."
msgstr ""

#. package does not exist
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1051
#: ../apport/ui.py:1051
msgid "The report belongs to a package that is not installed."
msgstr ""

#: ../apport/ui.py:1137
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1137
msgid "An error occurred while attempting to process this problem report:"
msgstr ""

#: ../apport/ui.py:1219
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1219
msgid "Could not determine the package or source package name."
msgstr ""

#: ../apport/ui.py:1237
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1237
msgid "Unable to start web browser"
msgstr ""

#: ../apport/ui.py:1238
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1238
#, python-format
msgid "Unable to start web browser to open %s."
msgstr ""

#: ../apport/ui.py:1338
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1338
#, python-format
msgid "Please enter your account information for the %s bug tracking system"
msgstr ""

#: ../apport/ui.py:1350
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1350
msgid "Network problem"
msgstr ""

#: ../apport/ui.py:1352
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1352
msgid ""
"Cannot connect to crash database, please check your Internet connection."
msgstr ""

#: ../apport/ui.py:1379
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1379
msgid "Memory exhaustion"
msgstr ""

#: ../apport/ui.py:1380
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1380
msgid "Your system does not have enough memory to process this crash report."
msgstr ""

#: ../apport/ui.py:1415
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1415
#, python-format
msgid ""
"The problem cannot be reported:\n"
"\n"
"%s"
msgstr ""

#: ../apport/ui.py:1471 ../apport/ui.py:1478
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1471
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1478
msgid "Problem already known"
msgstr ""

#: ../apport/ui.py:1472
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1472
msgid ""
"This problem was already reported in the bug report displayed in the web "
"browser. Please check if you can add any further information that might be "
"helpful for the developers."
msgstr ""

#: ../apport/ui.py:1479
#: ../debian/tmp/usr/lib/python2.7/dist-packages/apport/ui.py:1479
msgid "This problem was already reported to developers. Thank you!"
msgstr ""

#: ../debian/tmp/usr/share/apport/apportcheckresume.py:67
#: ../data/apportcheckresume.py:67
msgid ""
"This occurred during a previous suspend, and prevented the system from "
"resuming properly."
msgstr ""

#: ../debian/tmp/usr/share/apport/apportcheckresume.py:69
#: ../data/apportcheckresume.py:69
msgid ""
"This occurred during a previous hibernation, and prevented the system from "
"resuming properly."
msgstr ""

#: ../debian/tmp/usr/share/apport/apportcheckresume.py:74
#: ../data/apportcheckresume.py:74
msgid ""
"The resume processing hung very near the end and will have appeared to have "
"completed normally."
msgstr ""

#: ../bin/apport-cli.py:80 ../debian/tmp/usr/bin/apport-cli.py:80
msgid "Press any key to continue..."
msgstr ""

#: ../bin/apport-cli.py:87 ../debian/tmp/usr/bin/apport-cli.py:87
msgid "What would you like to do? Your options are:"
msgstr ""

#: ../bin/apport-cli.py:100 ../debian/tmp/usr/bin/apport-cli.py:100
#, python-format
msgid "Please choose (%s):"
msgstr ""

#: ../bin/apport-cli.py:164 ../debian/tmp/usr/bin/apport-cli.py:164
#, python-format
msgid "(%i bytes)"
msgstr ""

#: ../bin/apport-cli.py:195 ../debian/tmp/usr/bin/apport-cli.py:195
msgid ""
"After the problem report has been sent, please fill out the form in the\n"
"automatically opened web browser."
msgstr ""

#: ../bin/apport-cli.py:198 ../debian/tmp/usr/bin/apport-cli.py:198
#, python-format
msgid "&Send report (%s)"
msgstr ""

#: ../bin/apport-cli.py:202 ../debian/tmp/usr/bin/apport-cli.py:202
msgid "&Examine locally"
msgstr ""

#: ../bin/apport-cli.py:206 ../debian/tmp/usr/bin/apport-cli.py:206
msgid "&View report"
msgstr ""

#: ../bin/apport-cli.py:207 ../debian/tmp/usr/bin/apport-cli.py:207
msgid "&Keep report file for sending later or copying to somewhere else"
msgstr ""

#: ../bin/apport-cli.py:208 ../debian/tmp/usr/bin/apport-cli.py:208
msgid "Cancel and &ignore future crashes of this program version"
msgstr ""

#: ../bin/apport-cli.py:210 ../bin/apport-cli.py:287 ../bin/apport-cli.py:319
#: ../bin/apport-cli.py:340 ../debian/tmp/usr/bin/apport-cli.py:210
#: ../debian/tmp/usr/bin/apport-cli.py:287
#: ../debian/tmp/usr/bin/apport-cli.py:319
#: ../debian/tmp/usr/bin/apport-cli.py:340
msgid "&Cancel"
msgstr ""

#: ../bin/apport-cli.py:238 ../debian/tmp/usr/bin/apport-cli.py:238
msgid "Problem report file:"
msgstr ""

#: ../bin/apport-cli.py:244 ../bin/apport-cli.py:249
#: ../debian/tmp/usr/bin/apport-cli.py:244
#: ../debian/tmp/usr/bin/apport-cli.py:249
msgid "&Confirm"
msgstr ""

#: ../bin/apport-cli.py:248 ../debian/tmp/usr/bin/apport-cli.py:248
#, python-format
msgid "Error: %s"
msgstr ""

#: ../bin/apport-cli.py:255 ../debian/tmp/usr/bin/apport-cli.py:255
msgid ""
"The collected information can be sent to the developers to improve the\n"
"application. This might take a few minutes."
msgstr ""

#: ../bin/apport-cli.py:268 ../debian/tmp/usr/bin/apport-cli.py:268
msgid ""
"The collected information is being sent to the bug tracking system.\n"
"This might take a few minutes."
msgstr ""

#: ../bin/apport-cli.py:318 ../debian/tmp/usr/bin/apport-cli.py:318
msgid "&Done"
msgstr ""

#: ../bin/apport-cli.py:324 ../debian/tmp/usr/bin/apport-cli.py:324
msgid "none"
msgstr ""

#: ../bin/apport-cli.py:325 ../debian/tmp/usr/bin/apport-cli.py:325
#, python-format
msgid "Selected: %s. Multiple choices:"
msgstr ""

#: ../bin/apport-cli.py:341 ../debian/tmp/usr/bin/apport-cli.py:341
msgid "Choices:"
msgstr ""

#: ../bin/apport-cli.py:355 ../debian/tmp/usr/bin/apport-cli.py:355
msgid "Path to file (Enter to cancel):"
msgstr ""

#: ../bin/apport-cli.py:361 ../debian/tmp/usr/bin/apport-cli.py:361
msgid "File does not exist."
msgstr ""

#: ../bin/apport-cli.py:363 ../debian/tmp/usr/bin/apport-cli.py:363
msgid "This is a directory."
msgstr ""

#: ../bin/apport-cli.py:369 ../debian/tmp/usr/bin/apport-cli.py:369
msgid "To continue, you must visit the following URL:"
msgstr ""

#: ../bin/apport-cli.py:371 ../debian/tmp/usr/bin/apport-cli.py:371
msgid ""
"You can launch a browser now, or copy this URL into a browser on another "
"computer."
msgstr ""

#: ../bin/apport-cli.py:373 ../debian/tmp/usr/bin/apport-cli.py:373
msgid "Launch a browser now"
msgstr ""

#: ../bin/apport-cli.py:388 ../debian/tmp/usr/bin/apport-cli.py:388
msgid "No pending crash reports. Try --help for more information."
msgstr ""
